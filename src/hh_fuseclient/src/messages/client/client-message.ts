import { Message } from "../common/message";
import { MessageSerializer } from "../../util/message-serializer";

export abstract class ClientMessage extends Message {
    protected _serializer = new MessageSerializer();

    constructor(opcode: number) {
        super(opcode);
        this._serializer.writeBase64(this.opcode, 2);
    }

    abstract encode(): void;

    toRawPacket(): string {
        return this._serializer.toString();
    }
}

export namespace ClientMessage {
    export const TRY_LOGIN = 4;
    export const GET_INFO = 7;
    export const GET_CREDITS = 8;
    export const GET_AVAILABLE_SETS = 9;
    export const REGISTER = 43;
    export const TRYFLAT = 57;
    export const GOTOFLAT = 59;
    export const G_HMAP = 60;
    export const G_USRS = 61;
    export const G_OBJS = 62;
    export const G_ITEMS = 63;
    export const G_STAT = 64;
    export const WALK = 75;
    export const WAVE = 94;
    export const GET_AVAILABLE_BADGES = 157;
    export const GET_SESSION_PARAMETERS = 181;

    export const PONG = 196;
}