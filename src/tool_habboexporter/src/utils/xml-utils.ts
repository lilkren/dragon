import * as parser from "fast-xml-parser";

export namespace XMLUtils {
    export const parseXml = (xmlData: string): any => {
        const options = {
            attributeNamePrefix: "",
            textNodeName: "#text",
            ignoreAttributes: false,
            ignoreNameSpace: true,
            allowBooleanAttributes: true,
            parseNodeValue: true,
            parseAttributeValue: true,
            trimValues: true,
            cdataTagName: "__cdata",
            cdataPositionChar: "\\c",
            localeRange: "",
            parseTrueNumberOnly: false,
        };

        const validation = parser.validate(xmlData);

        if (validation === true) {
            const jsonObj = parser.parse(xmlData, options);
            return jsonObj;
        } else {
            console.log(validation)
        }

        return null;
    };

    export const parseXmlArray = (data: any): any[] => {
        if (data == null) {
            return [];
        }
        if (data instanceof Array) {
            return data;
        }
        return [data];
    };
}