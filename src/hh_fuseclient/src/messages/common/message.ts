export class Message {
    opcode: number;

    constructor(opcode: number) {
        this.opcode = opcode;
    }
}