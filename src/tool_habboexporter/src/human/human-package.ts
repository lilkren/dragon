import { injectable, inject } from "inversify";
import { Settings } from "../settings";
import mkdirp from "mkdirp";
import fs from "fs";
import { SWFUtils } from "../utils/swf-utils";
import { XMLUtils } from "../utils/xml-utils";
import JSZip from "jszip";
import { HumanAssetDictionary } from "hh_shared";

@injectable()
export class HumanPackage {
    @inject(Settings)
    private settings: Settings;

    async process() {
        if(this.settings.extractHuman) {
            await this.extract();
        }

        if(this.settings.convertHuman) {
            await this.convert();
        }

        if(this.settings.packageHuman) {
            await this.package();
        }
    }

    private async extract() {
        console.log("Extracting human packages...");

        const swfsPath = this.settings.humanDownloadPath;
        const outputPathPrefix = this.settings.humanExtractPath;

        if (!fs.existsSync(swfsPath)) {
            throw new Error(`Could not extract human packages because path '${swfsPath}' does not exist.`);
        }

        const swfs = fs.readdirSync(swfsPath).filter(fn => fn.endsWith(".swf"));

        for (const swfFileName of swfs) {
            const swfPath = `${swfsPath}/${swfFileName}`;
            const swfName = swfFileName.substring(0, swfFileName.length - 4);
            console.log(`Extracting human package ${swfName}...`);
            const outputPath = `${outputPathPrefix}/${swfName}`;
            mkdirp.sync(outputPath);
            await SWFUtils.extractSwf(swfPath, outputPath, swfName.length + 1);
            console.log(`Extracted human package ${swfName}.`);

        }

        console.log("Extracted human packages.");
    }

    private async convert() {
        console.log("Converting figure packages...");
        const extractedPath = this.settings.humanExtractPath;

        if (!fs.existsSync(extractedPath)) {
            throw new Error(`Could not convert human packages because path '${extractedPath}' does not exist.`);
        }

        const contents = fs.readdirSync(extractedPath);
        const packages = contents.filter((val) => !val.startsWith("."));

        for (const pkgName of packages) {
            await this.convertPackage(pkgName);
        }

        console.log("Converted figure packages.");

    }

    private async convertPackage(pkgName: string) {
        console.log(`Converting figure package ${pkgName}...`);
        const extractedPath = `${this.settings.humanExtractPath}/${pkgName}`;
        const outputPath = `${this.settings.humanConvertPath}/${pkgName}`;
        const rawManifest = fs.readFileSync(`${extractedPath}/manifest.xml`, "utf-8");
        const manifest = XMLUtils.parseXml(rawManifest);
        const library = manifest.manifest.library;
        const assets = XMLUtils.parseXmlArray(library.assets.asset);

        mkdirp.sync(outputPath);

        const figureAssetDict: HumanAssetDictionary = {};

        for (const rawAsset of assets) {
            if (rawAsset.mimeType === "image/png") {
                const name = rawAsset.name;
                const imageName = `${name}.png`;

                let offsetX = 0;
                let offsetY = 0;

                if (rawAsset.param && rawAsset.param.value) {
                    const splitOffset = rawAsset.param.value.split(",");
                    if (splitOffset && splitOffset.length) {
                        offsetX = parseInt(splitOffset[0]);
                        offsetY = parseInt(splitOffset[1]);
                    }
                }

                try {
                    fs.copyFileSync(`${extractedPath}/${imageName}`, `${outputPath}/${imageName}`);

                    figureAssetDict[rawAsset.name] = {
                        exists: true,
                        name: name,
                        offsetX: offsetX,
                        offsetY: offsetY,
                        flipH: false,
                        flipV: false
                    }
                } catch (e) {
                }
            }
        }

        if (library.aliases) {
            const aliases = XMLUtils.parseXmlArray(library.aliases.alias);
            for (const rawAlias of aliases) {
                const realAsset = figureAssetDict[rawAlias.link];
                if (realAsset) {
                    figureAssetDict[rawAlias.name] = {
                        exists: false,
                        name: rawAlias.name,
                        source: realAsset.name,
                        offsetX: realAsset.offsetX,
                        offsetY: realAsset.offsetY,
                        flipH: rawAlias.fliph,
                        flipV: rawAlias.flipv
                    }
                }
            }
        }

        const newManifest = JSON.stringify(figureAssetDict, null, 2);
        fs.writeFileSync(`${outputPath}/figurepackage.json`, Buffer.from(newManifest, "utf-8"));

        console.log(`Converted figure package ${pkgName}...`);
    }

    private async package() {
        console.log("Packaging human packages...");

        const convertedPath = this.settings.humanConvertPath;
        const outputPath = this.settings.humanPackagesPath;
        mkdirp.sync(outputPath);

        if (!fs.existsSync(convertedPath)) {
            throw new Error(`Could not package human package because '${convertedPath}' does not exist.`);
        }

        const folders = fs.readdirSync(convertedPath).filter(name => !name.startsWith("."));

        for(const folder of folders) {
            const srcPath = `${convertedPath}/${folder}`;
            if(fs.statSync(srcPath).isDirectory()) {
                console.log(`Packaging human package ${folder}...`);
                const fileContents = fs.readdirSync(srcPath);
                const files = fileContents.filter((val) => !val.startsWith(".") && !val.startsWith("__"));
                const zip = new JSZip();
                for(const file of files) {
                    const fileContents = fs.readFileSync(`${srcPath}/${file}`);
                    zip.file(file, fileContents);
                }

                const arrayBuffer = await zip.generateAsync({type: "arraybuffer" });
                fs.writeFileSync(`${outputPath}/${folder}.zip`, Buffer.from(arrayBuffer));
                console.log(`Packaged human package ${folder}.`);

            }
        }

        console.log("Packaged human packages.");

    }
}