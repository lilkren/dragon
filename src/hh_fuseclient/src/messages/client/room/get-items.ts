import { ClientMessage } from "../client-message";
import { ServerMessage } from "../../server/server-message";

export class GetItems extends ClientMessage {
    constructor() {
        super(ClientMessage.G_ITEMS);
    }

    encode() {
    }
}