export enum WorldSize {
    ICON = 1,
    SMALL = 32,
    LARGE = 64
}