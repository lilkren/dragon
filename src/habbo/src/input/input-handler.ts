import { Point, ButtonModifiers, MouseMoveEvent, MouseClickEvent, MouseDoubleClickEvent, InputEvent } from "hh_shared";

export class InputHandler {
    private isMouseDragging: boolean;
    private eventHandler: (event: InputEvent) => void;

    init(domElement: HTMLElement, eventHandler: (event: InputEvent) => void) {
        this.eventHandler = eventHandler;

        domElement.addEventListener("mousemove", (evt) => {
            const position = Point.of(evt.x, evt.y);
            const buttonMods = new ButtonModifiers(evt.ctrlKey, evt.shiftKey, evt.altKey);
            const event = new MouseMoveEvent(position, buttonMods, this.isMouseDragging);
            this.emitEvent(event);
        }, false);

        domElement.addEventListener("click", (evt) => {
            const position = Point.of(evt.x, evt.y);
            const buttonMods = new ButtonModifiers(evt.ctrlKey, evt.shiftKey, evt.altKey);
            const event = new MouseClickEvent(position, buttonMods);
            this.emitEvent(event);
        }, false);

        domElement.addEventListener("dblclick", (evt) => {
            const position = Point.of(evt.x, evt.y);
            const buttonMods = new ButtonModifiers(evt.ctrlKey, evt.shiftKey, evt.altKey);
            const event = new MouseDoubleClickEvent(position, buttonMods);
            this.emitEvent(event);
        }, false);

        domElement.addEventListener("mousedown", (evt) => {
            this.isMouseDragging = true;
        }, false);

        domElement.addEventListener("mouseup", (evt) => {
            this.isMouseDragging = false;
        }, false);
    }

    private emitEvent(inputEvent: InputEvent) {
        this.eventHandler(inputEvent);
    }

}