import { ServerMessage } from "../server-message";
import { MessageDeserializer } from "../../../util/message-deserializer";

export class RoomReady extends ServerMessage {
    model: string;
    roomId: string;

    constructor(deserializer: MessageDeserializer) {
        super(ServerMessage.ROOM_READY, deserializer);
    }

    decode() {
       this.model = this._deserializer.readString();
       this._deserializer.readString();
       this.roomId = this._deserializer.readInt().toString(); 
    }
}