import { ServerMessage } from "../server-message";
import { MessageDeserializer } from "../../../util/message-deserializer";

export class LocalisedError extends ServerMessage {
    errorMsg: string;

    constructor(deserializer: MessageDeserializer) {
        super(ServerMessage.LOCALISED_ERROR, deserializer);
    }

    decode() {
        this.errorMsg = this._deserializer.peekBody();
    }
}