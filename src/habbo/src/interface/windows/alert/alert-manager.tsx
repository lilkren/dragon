import { AlertWindow } from "./alert-window";
import React from "react";

export type AlertCallback = (alerts: JSX.Element[]) => void;

export class AlertManager {
    private _counter = 0;
    private _alerts: {[value: number]: JSX.Element}=  {}
    private _callback: AlertCallback;

    init(callback: AlertCallback) {
        this._callback = callback;
    }

    fireAlert(title: string, content: string) {
        const id = ++this._counter;
        const aw = <AlertWindow alertId={id} title={title}>{content}</AlertWindow>;
        this._alerts[id] = aw;
        this.updateList();
    }

    dismissAlert(id: number) {
        this._alerts[id] = undefined;
        this.updateList();
    }

    updateList() {
        this._callback(Object.values(this._alerts));
    }
}