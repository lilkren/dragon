import { ServerMessage } from "../server-message";
import { MessageDeserializer } from "../../../util/message-deserializer";

export class UserObject extends ServerMessage {
    id: string;
    name: string;
    figure: string;
    gender: string;
    motto: string;
    tickets: number;
    poolFigure: string;
    film: number;

    constructor(deserializer: MessageDeserializer) {
        super(ServerMessage.USER_OBJECT, deserializer);
    }

    decode() {
        this.id = this._deserializer.readString();
        this.name = this._deserializer.readString();
        this.figure = this._deserializer.readString();
        this.gender =  this._deserializer.readString();
        this.motto = this._deserializer.readString();
        this.tickets = this._deserializer.readInt();
        this.poolFigure = this._deserializer.readString();
        this.film = this._deserializer.readInt();
    }
}