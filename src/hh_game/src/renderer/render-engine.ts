import * as PIXI from "pixi.js";
import { Point, ColorUtils } from "hh_shared";
import TWEEN from "@tweenjs/tween.js";

export class RenderEngine {
    private _cameraPosition: Point = Point.of(450, 250);

    private _logicRenderer: PIXI.Renderer;
    private _visualRenderer: PIXI.Renderer;
    private _logicStage: PIXI.Container;
    private _visualStage: PIXI.Container;

    private _domElement: HTMLElement;
    private _gameLoop: (delta: number) => void;
    private _lastTime: number = 0;

    init(domElement: HTMLElement, gameLoop: (delta: number) => void) {
        this._domElement = domElement;
        this._gameLoop = gameLoop;

        PIXI.utils.skipHello();
        PIXI.settings.SCALE_MODE = PIXI.SCALE_MODES.NEAREST;
        PIXI.settings.ROUND_PIXELS = true;

        const pixiSettings = {
            autoStart: false,
            antialias: false,
            preserveDrawingBuffer: true,
            transparent: true
        }

        this._logicRenderer = new PIXI.Renderer(pixiSettings);
        this._visualRenderer = new PIXI.Renderer(pixiSettings);

        this._logicStage = new PIXI.Container();

        this._visualStage = new PIXI.Container();

        this.createCanvas();
    }

    start() {
        this.update(0);
    }

    private createCanvas() {
        this._domElement.appendChild(this._visualRenderer.view);

    }

    private destroyCanvas() {
        if (this._domElement.contains(this._visualRenderer.view)) {
            this._domElement.removeChild(this._visualRenderer.view);
        }
    }

    private update(time: number) {
        const delta = time - this._lastTime;
        this._lastTime = time;


        if (this.visualRenderer.width != this._domElement.clientWidth || this.visualRenderer.height != this._domElement.clientHeight) {
            this.visualRenderer.resize(this._domElement.clientWidth, this._domElement.clientHeight);
        }

        if (this.logicRenderer.width != this._domElement.clientWidth || this.logicRenderer.height != this._domElement.clientHeight) {
            //this.logicRenderer.resize(this._domElement.clientWidth, this._domElement.clientHeight);
        }

        TWEEN.update(time)

        this._gameLoop(delta);

        this.logicStage.position.set(this.camera.x, this.camera.y);
        this.visualStage.position.set(this.camera.x, this.camera.y);

        this._cameraPosition.x = (this.visualRenderer.width / 2) ;
        this._cameraPosition.y = (this.visualRenderer.height / 2)  - (this.visualStage.height / 2);


        this._logicStage.sortChildren();
        this._visualStage.sortChildren();

        this._logicRenderer.render(this._logicStage);
        this._visualRenderer.render(this._visualStage);


        requestAnimationFrame((time) => this.update(time));
    }

    renderToTexture(dispObj: PIXI.DisplayObject): PIXI.Texture {
        const texture = this.visualRenderer.generateTexture(
            dispObj,
            PIXI.SCALE_MODES.NEAREST,
            1
        );

        return texture;
    }

    selectColorLogic(mouse: Point): number {
        const pixels = this.logicRenderer.extract.canvas(undefined);
        const rawPixels = pixels.getContext("2d").getImageData(mouse.x, mouse.y, 1, 1);

        const [r, g, b, a] = rawPixels.data;
        if (a === 255) {
            return ColorUtils.rgb2hex({ r: r / 255, g: g / 255, b: b / 255 });
        }

        return -1;
    }

    get visualStage(): PIXI.Container {
        return this._visualStage;
    }

    get visualRenderer(): PIXI.Renderer {
        return this._visualRenderer;
    }

    get logicStage(): PIXI.Container {
        return this._logicStage;
    }

    get logicRenderer(): PIXI.Renderer {
        return this._logicRenderer;
    }

    get camera(): Point {
        return this._cameraPosition;
    }
}