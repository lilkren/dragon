# Credits

Written by Joeh/Asgard.

Significant inspiration and concepts were taken from various projects.

Special thanks:
 * bobba.io (https://github.com/Josedn/bobba_client).
 * Kepler (https://github.com/Quackster/Kepler)
 * Habbo CCT source (https://github.com/Quackster/habbo_src)