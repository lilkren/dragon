import * as PIXI from "pixi.js";
import { WorldSize, ModelPackageData } from "hh_shared";
import { WorldHelpers } from "../world/world-helpers";
import { TileGraphics } from "./tile-graphics";
import { Game } from "../game";
import { ModelLoader } from "./model-loader";
import { ModelHelpers } from "./model-helpers";

export default class ModelManager {
    private _textureCache = new Map<string, PIXI.Texture>();
    private _roomPackages = new Map<string, Promise<ModelPackageData>>();
    private _roomFloormaps = new Map<string, number[][]>();

    private _roomLoader = new ModelLoader();

    async start() {
        await this.getModelPackage("hh_room_private");
    }

    getModelPackage(roomType: string): Promise<ModelPackageData> {
        let roomPack = this._roomPackages.get(roomType);
        if(!roomPack) {
            roomPack = this._roomLoader.loadModelPackage(roomType);
            this._roomPackages.set(roomType, roomPack);

            // Extract floor maps
            roomPack.then((model) => {
                for(const key in model.floorMaps) {
                    const value = model.floorMaps[key];
                    this._roomFloormaps.set(key, ModelHelpers.textMapToArray(value));
                }
            })
        }
        return roomPack;
    }

    getModelFloormap(modelName: string) {
        return this._roomFloormaps.get(modelName);
    } 

    getTileTexture(worldSize: WorldSize, tileColor: number = 0x989865, tileThickness: number = -1): PIXI.Texture {
        if(tileThickness === -1) {
            tileThickness = worldSize == WorldSize.LARGE ? 7 : 3;
        }

        const key = `tile_normal_${worldSize}_${tileColor}_${tileThickness}`;

        if (this._textureCache.has(key)) {
            return this._textureCache.get(key);
        } else {
            const tileDimensions = WorldHelpers.getTileDimensions(worldSize);
            const tileView = TileGraphics.generateFloorTile(tileDimensions, tileThickness, tileColor);
            const texture = Game.instance.renderer.renderToTexture(tileView);
            this._textureCache.set(key, texture);
            return texture;
        }
    }

    getTileHighlightTexture(worldSize: WorldSize, highlightColor: number = 0xFFFFFF, highlightThickness: number = 1): PIXI.Texture {
        const key = `tile_highlight_${worldSize}_${highlightColor}_${highlightThickness}`;

        if (this._textureCache.has(key)) {
            return this._textureCache.get(key);
        } else {
            const tileDimensions = WorldHelpers.getTileDimensions(worldSize);
            const tileView = TileGraphics.generateHighlightTile(tileDimensions, highlightColor, highlightThickness);
            const texture = Game.instance.renderer.renderToTexture(tileView);
            this._textureCache.set(key, texture);
            return texture;
        }
    }
}