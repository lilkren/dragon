import React from "react";
import "./hotel-view-screen.css";
import { BottomBarComponent } from "../../components/bottom-bar/bottom-bar-component";

export const HotelViewScreen = () => {
    return (
        <div className="gameScreen hotelViewScreen">
            <div className="hotelView">
                <BottomBarComponent></BottomBarComponent>
            </div>
        </div>
    );
}