import { RoomModel } from "./room-model";
import { HumanInstance, HumanInstanceOpts, HumanInstanceState } from "../human/human-instance";
import { HumanState } from "../human/human-renderer";

export class RoomHumans {
    private _activeAvatars: {[key: string]: HumanInstance} = {}


    createOrUpdateUser(humanOptions: HumanInstanceOpts, state?: HumanInstanceState): HumanInstance {
        const userAvatar = this._activeAvatars[humanOptions.instanceId];
        
        if(userAvatar) {
            userAvatar.setState(state ?? {});
        } else {
            const newUserAvatar = new HumanInstance(
                humanOptions,
                state ?? {}
            );
            this._activeAvatars[humanOptions.instanceId] = newUserAvatar;
        }
       

        return userAvatar;
    }

    updateStatus(id: string, state: HumanInstanceState) {
        const human = this._activeAvatars[id];
        human?.setState(state);
    }

    destroyUser(id: string) {
        this._activeAvatars[id]?.unload();
        this._activeAvatars[id] = undefined;
    }

    update(delta: number) {
        for (const humanKey in this._activeAvatars) {
            this._activeAvatars[humanKey]?.update(delta);
        }
    }

    onClick(selectedColor: number): boolean {
        for (const humanKey in this._activeAvatars) {
            const human = this._activeAvatars[humanKey];
            if(human?.isHitDetected(selectedColor)) return true;
        }
        return false;
    }

    onDoubleClick(selectedColor: number): boolean {
        for (const humanKey in this._activeAvatars) {
            const human = this._activeAvatars[humanKey];
            if(human?.isHitDetected(selectedColor)) return true;
        }
        return false;
    }

    unload() {
        for (const humanKey in this._activeAvatars) {
           this.destroyUser(humanKey);
        }
        this._activeAvatars = {};
    }
}