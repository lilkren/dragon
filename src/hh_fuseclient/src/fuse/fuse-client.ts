import { ClientSocket } from "./client-socket";
import { MessageFramer } from "../util/message-framer";
import { MessageDeserializer } from "../util/message-deserializer";
import { ClientMessage } from "../messages/client/client-message";
import { ServerMessageDeserializerMap } from "../messages/server/server-message-deserializer-map";
export type MessageListener = (msg: any) => void;


export class FuseClient {
    private _socket = new ClientSocket(this);

    private _listeners = new Map<number, MessageListener>()
    private _mapper = new ServerMessageDeserializerMap();

    private _requestedDisconnect = false;

    private _messageFramer = new MessageFramer((msg) => this.onFullMessage(msg));

    async connect() {
        this._requestedDisconnect = false;
        return this._socket.connect("ws://localhost:8990");
    }

    async disconnect() {
        console.log("REQUESTED")
        this._requestedDisconnect = true;
        this._socket.disconnect();
    }

    send(msg: ClientMessage) {
        msg.encode();
        const raw = msg.toRawPacket();
        this._socket.send(raw);
        console.log(raw);
        console.log(msg);
    }

    onMessagePart(raw: Blob) {
        const fr = new FileReader()
        fr.readAsText(raw, 'utf-8')
        fr.onloadend = () => {
            this._messageFramer.onAdditional(fr.result.toString())
        }
    }

    onFullMessage(raw: string) {
        const parser = new MessageDeserializer(raw);
        const msg = this._mapper.doDecoding(parser);

        console.log(raw);
        console.log(msg);

        if(msg) {
            const handler = this._listeners.get(msg.opcode);
            if(handler) {
                handler(msg);
            }
        }
    }

    onDisconnected() {
        if(!this._requestedDisconnect) {
            throw new Error("Disconnected");
        }
    }

    onConnected() {

    }

    registerListener(opcode: number, listener: MessageListener) {
        this._listeners.set(opcode, listener)
    }

    deregisterListener(opcode: number) {
        this._listeners.delete(opcode)
    }
}