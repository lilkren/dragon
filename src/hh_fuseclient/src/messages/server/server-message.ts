import { MessageDeserializer } from "../../util/message-deserializer";
import { Message } from "../common/message";

export abstract class ServerMessage extends Message {
    protected _deserializer: MessageDeserializer;

    constructor(opcode: number, deserializer: MessageDeserializer) {
        super(opcode);
        this._deserializer = deserializer;
    }

    abstract decode(): void;
}

export namespace ServerMessage {
    export const HELLO = 0;
    export const FUSE_RIGHTS = 2;
    export const LOGIN_OK = 3;
    export const USER_OBJECT = 5;
    export const CREDIT_BALANCE = 6;
    export const AVAILABLE_SETS = 8;
    export const HOTEL_VIEW = 18;
    export const USER_OBJECTS = 28;
    export const USER_LOGOUT = 29;

    export const HEIGHTMAP = 31;
    export const ACTIVE_OBJECTS = 32;
    export const LOCALISED_ERROR = 33;
    export const USER_STATUSES = 34;
    export const FLAT_LETIN = 41;
    export const PING = 50;
    export const ROOM_READY = 69;
    export const AVAILABLE_BADGES = 229;

}