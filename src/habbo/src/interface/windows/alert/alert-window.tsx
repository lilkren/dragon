import "./alert-window.css";
import { WindowComponent } from "../../components/window/window-component";
import React, { FunctionComponent, useRef, useEffect } from "react";
import { UIManager } from "../../ui-manager";

export type AlertWindowProps = {
    alertId: number,
    title: string
}

export const AlertWindow: FunctionComponent<AlertWindowProps> = ({ alertId, title, children }) => {
    const onDismiss = () => {
        UIManager.instance.alerts.dismissAlert(alertId);
    }

    const okButton = useRef<HTMLButtonElement>();

    useEffect(() => {
        okButton.current.focus();
    }, []);

    return (<div className="centerOnScreen alertContainer">
        <WindowComponent
            className="alertWindow"
            title={title}
            titleFont="Volter-Bold"
            customStyle={{
                textAlign: "center",
                fontSize: "9pt",
                color: "white",
                position: "absolute",
            }}
        >
            <WindowComponent
                backgroundColor="white"
                borderBlur={0}
                borderSpread={0}
                customStyle={{
                    textAlign: "center",
                    fontSize: "9pt",
                    color: "black",
                    paddingTop: 5
                }}
            >
                <p>
                    {children}

                </p>
                <button ref={okButton} onClick={() => onDismiss()}>OK</button>
            </WindowComponent>
        </WindowComponent>
    </div>

    );
};