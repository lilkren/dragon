const DELIMITER = String.fromCharCode(1)

type OnMessageComplete = (msg: string) => void;

export class MessageFramer {
    private _messageSoFar: string = "";
    private _onComplete: OnMessageComplete;

    constructor(onComplete: OnMessageComplete) {
        this._onComplete = onComplete;
    }

    onAdditional(data: string) {
        for(const chr of data) {
            if(chr === DELIMITER) {
                this._onComplete(this._messageSoFar);
                this._messageSoFar = "";
            } else {
                this._messageSoFar += chr;
            }
        }
    }
}