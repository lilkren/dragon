import { WorldSize } from "hh_shared";

export type HumanPart = { type: string, id: string, colors: string[] };

export namespace HumanHelpers {
    export const generateResourceName = (
        worldSize: WorldSize,
        action: string,
        type: string,
        partId: string | number,
        direction: string | number,
        frame: string | number
    ): string => {
        let resourceName = worldSizeToHumanSize(worldSize)
        resourceName += "_";
        resourceName += action;
        resourceName += "_";
        resourceName += type;
        resourceName += "_";
        resourceName += partId;
        resourceName += "_";
        resourceName += direction;
        resourceName += "_";
        resourceName += frame;
        return resourceName;
    }

    export const worldSizeToHumanSize = (worldSize: WorldSize): string => {
        switch (worldSize) {
            case WorldSize.LARGE:
                return "h";
            case WorldSize.SMALL:
                return "sh";
        }
    }

    export const extractFigureParts = (figure: string) => {
        const newFigure: { [id: string]: HumanPart } = {};
        const parts: HumanPart[] = [];

        for (let part of figure.split(".")) {
            const data = part.split("-");
            const figurePart: HumanPart = { type: data[0], id: data[1], colors: [data[2]] };
            if (data[3] != null) {
                figurePart.colors.push(data[3]);
            }
            newFigure[figurePart.type] = figurePart;
        }

        for (let part in newFigure) {
            parts.push(newFigure[part]);
        }
        return parts;
    }
}