import { ServerMessage } from "../server-message";
import { MessageDeserializer } from "../../../util/message-deserializer";

export class Hello extends ServerMessage {
    constructor(deserializer: MessageDeserializer) {
        super(ServerMessage.HELLO, deserializer);
    }

    decode() {
        
    }
}