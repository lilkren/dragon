import { WorldSize } from "../world/world-common";
import { FurniDirection } from "./furni-common";
import { IsometricDimensions } from "../misc/math";

export type FurniAsset = {
    name: string;
    x: number;
    y: number;
    exists: boolean;
    flipH: boolean;
    source?: string;
};

export type FurniAssetDictionary = { [key: string]: FurniAsset };

export type FurniVisualizationColorData = {
    layerId: number,
    color: string,
};

export type FurniVisualizationColor = {
    [colorId: string]: FurniVisualizationColorData[],
};

export type FurniVisualizationLayerData = {
    layerId: number,
    ignoreMouse: boolean,
    ink?: string,
    alpha?: number,
    z?: number,
};

export type FurniAnimationLayer = {
    layerId: number,
    frameSequence: number[][],
};

export type FurniVisualizationAnimation = {
    id: number,
    transitionTo?: number,
    layers: FurniAnimationLayer[],
};

export type FurniVisualiazationDictionary = { [key in WorldSize]: FurniVisualiazation };

export type FurniVisualizationAnimationDictionary = { [animationId: number]: FurniVisualizationAnimation };

export type FurniVisualizationLayerDataDictionary = { [directionId: number]: FurniVisualizationLayerData[] };

export type FurniVisualiazation = {
    size: WorldSize;
    layerCount: number;
    angle: number;
    directions: FurniVisualizationLayerDataDictionary,
    layers?: FurniVisualizationLayerData[],
    colors?: FurniVisualizationColor,
    animations?: FurniVisualizationAnimationDictionary,
};

export declare type FurniBehaviour = {
    logic: string,
    visual: string
};

export declare type FurniLogic = {
    behavior: FurniBehaviour
    dimensions: IsometricDimensions;
    directions: FurniDirection[];
};

export type FurniDefinition = {
    assets: FurniAssetDictionary;
    logic: FurniLogic;
    visualization: FurniVisualiazationDictionary;
};