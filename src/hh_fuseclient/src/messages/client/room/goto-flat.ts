import { ClientMessage } from "../client-message";

export class GotoFlat extends ClientMessage {
    roomId: string;

    constructor(roomId?: string) {
        super(ClientMessage.GOTOFLAT);
        this.roomId = roomId;
    }

    encode() {
        this._serializer.writeRaw(this.roomId);
    }
}