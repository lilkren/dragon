import { IsometricDimensions } from "../misc/math";

export enum FurniType {
    WALL = "wall",
    FLOOR = "floor"
}

export type FurniDataList = FurniDataEntry[];

export type FurniDataEntry = {
    asset: string,
    type: FurniType,
    typeData: FurniDataFloorType | FurniDataWallType,
    name: string,
    description: string,
    [key: string]: any
}

export type FurniDataFloorType = {
    dimensions: IsometricDimensions,
    defaultDir: number
}

export type FurniDataWallType = {

}