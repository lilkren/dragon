import { ServerMessage, FuseClient, ServerMessages, ClientMessage, ClientMessages } from "hh_fuseclient";
import { Game } from "../game";

export class MiscSystem {
    start() {
        Game.instance.client.registerListener(ServerMessage.LOCALISED_ERROR, (msg) => this.onLocalisedError(msg));
        Game.instance.client.registerListener(ServerMessage.PING, (_) => this.onPing());
    }

    private onLocalisedError(msg: ServerMessages.LocalisedError) {
        if(msg.errorMsg.toLowerCase().includes("login incorrect")) {
            Game.instance.handshake.loginFailed();
        }
    }

    private onPing() {
        Game.instance.client.send(new ClientMessages.Pong())
    }
}