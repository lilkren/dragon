import * as PIXI from "pixi.js";
import { RoomModel } from "./room-model";
import { Game } from "../game";
import { IsometricPoint, Point } from "hh_shared";
import { WorldHelpers, WorldConsts } from "../world/world-helpers";
import { ClientMessages } from "hh_fuseclient";

export class RoomFloor {
    private _drawnTiles: PIXI.Sprite[] = [];

    private _model: RoomModel;

    private _selectedTile: PIXI.Sprite;

    roomLoaded(model: RoomModel) {

        this._model = model;
        
        this._selectedTile = new PIXI.Sprite(Game.instance.models.getTileHighlightTexture(model.worldSize));
        this._selectedTile.visible = false;

        Game.instance.renderer.visualStage.addChild(this._selectedTile);

        for (let i = 0; i < model.mapSize.width; i++) {
            for (let j = 0; j < model.mapSize.height; j++) {
                if (model.isValidTile(i, j)) {
                    const height = model.floorMap[i][j];
                    const isoPoint = IsometricPoint.of(i, j, height);
                    const position = WorldHelpers.tileToLocal(model.worldSize, isoPoint);
                    const texture = Game.instance.models.getTileTexture(model.worldSize);
                    const sprite = new PIXI.Sprite(texture);
                    sprite.x = position.x;
                    sprite.y = position.y;
                    sprite.zIndex = WorldHelpers.calculateZIndex(isoPoint, WorldConsts.PRIORITIY_FLOOR);
                    this._drawnTiles.push(sprite);
                    Game.instance.renderer.visualStage.addChild(sprite);
                }
            }
        }
    }

    unload() {
        for (const tile of this._drawnTiles) {
            Game.instance.renderer.visualStage.removeChild(tile);
            tile.destroy();
        }
        this._drawnTiles = [];

        if (this._selectedTile) {
            Game.instance.renderer.visualStage.removeChild(this._selectedTile);
            this._selectedTile.destroy();
            this._selectedTile = undefined;
        }
    }

    updateSelectedTile(mousePosition: Point) {
        const hovered = this.calculateHoveredTile(mousePosition);
        if (hovered) {
            const screenPosition = WorldHelpers.tileToLocal(this._model.worldSize, hovered);
            this._selectedTile.x = screenPosition.x;
            this._selectedTile.y = screenPosition.y;
            this._selectedTile.zIndex = WorldHelpers.calculateZIndex(hovered, WorldConsts.PRIORITIY_FLOOR_SELECT);
            this._selectedTile.visible = true;
        } else {
            this._selectedTile.visible = false;
        }
    }

    onClick(mousePosition: Point): boolean {
        const hovered = this.calculateHoveredTile(mousePosition);
        if (hovered) {
            Game.instance.client.send(new ClientMessages.Walk(hovered.x, hovered.y));
        }
        return true;
    }

    private calculateHoveredTile(mousePosition: Point) {
        const model = this._model;

        for (let i = WorldConsts.TILE_MAX_Z; i >= 0; i -= WorldConsts.TILE_STEP) {
            const isoPosition = WorldHelpers.localToTile(this._model.worldSize, Point.of(mousePosition.x, mousePosition.y), i);
            if (model.isValidTile(isoPosition.x, isoPosition.y) && model.floorMap[isoPosition.x][isoPosition.y] === isoPosition.z) {
                return isoPosition;
            }
        }
    }
}