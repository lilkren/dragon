export * from "./game-events";
import StrictEventEmitter from "strict-event-emitter-types";
import { EventEmitter } from 'events';
import { GameEvents } from "./game-events";
import { RenderEngine } from "./renderer/render-engine";
import { FuseClient } from "hh_fuseclient";
import { MiscSystem } from "./misc/misc-system";
import { HandshakeSystem } from "./misc/handshake-system";
import { UserSystem } from "./user/user-system";
import { HumanManager } from "./human/human-manager";
import ModelManager from "./model/model-manager";
import { RoomSystem } from "./room/room-system";
import { InputEvent, FetchCommon } from "hh_shared";
import { FurniManager } from "./furni/furni-manager";
import { GameSettings } from "./game-settings";

export class Game {
    private _settings: GameSettings;

    // Core systems
    private _eventEmitter: StrictEventEmitter<EventEmitter, GameEvents> = new EventEmitter();
    private _renderEngine = new RenderEngine();
    private _fuseClient = new FuseClient();
    private _humanManager = new HumanManager();
    private _modelManager = new ModelManager();
    private _furniManager = new FurniManager();


    // Reactive systems
    private _handshakeSystem = new HandshakeSystem();
    private _miscSystem = new MiscSystem();
    private _userSystem = new UserSystem();
    private _roomSystem = new RoomSystem();

    constructor() {

    }

    init(domElement: HTMLElement) {
        this._renderEngine.init(domElement, (delta) => this.update(delta));
    }

    onInput(input: InputEvent) {
        this._roomSystem.engine.onInput(input);
    }

    async start() {
        this.events.emit("GameLoading");
        
        this._settings = await FetchCommon.fetchJSON("./settings.json");

        // Reactive systems
        this._miscSystem.start();
        this._handshakeSystem.start();
        this._userSystem.start();
        this._roomSystem.start();

        // Core systems
        await this._humanManager.start();
        await this._modelManager.start();
        await this._furniManager.start();

        this._renderEngine.start();

        this.events.emit("GameLoaded");

    }

    update(delta: number) {
        this._userSystem.update(delta);
        this._roomSystem.engine.update(delta);
    }


    get events() {
        return this._eventEmitter;
    }

    get client() {
        return this._fuseClient;
    }

    get handshake() {
        return this._handshakeSystem;
    }

    get user() {
        return this._userSystem;
    }

    get room() {
        return this._roomSystem;
    }

    get humans() {
        return this._humanManager;
    }

    get renderer() {
        return this._renderEngine;
    }

    get models() {
        return this._modelManager;
    }

    get furni() {
        return this._furniManager;
    }

    get settings() {
        return this._settings;
    }

    private static _instance: Game;
    static get instance() {
        if (!this._instance) this._instance = new Game();
        return this._instance;
    }
}