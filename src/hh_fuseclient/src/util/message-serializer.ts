import { Base64Encoding, VL64Encoding } from "./special-encoding";

export class MessageSerializer {
    private _body: string = "";

    writeRaw(str: string) {
        this._body += str;
    }

    writeBase64(i: number, length: number) {
        this._body += Base64Encoding.encode(i, length);
    }

    writeInt(i: number) {
        this._body += VL64Encoding.encode(i);
    }

    writeString(str: string) {
        this.writeBase64(str.length, 2);
        this.writeRaw(str);
    }

    toString() {
        const lengthHeader = Base64Encoding.encode(this._body.length, 3);
        return lengthHeader + this._body;
    }
}