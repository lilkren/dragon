import { WorldSize, FurniDirection, FurniVisualiazation } from "hh_shared";

export type FurniState = { count: number, transition?: number, transitionTo?: number };
export type FurniStateDictionary = { [key: number]: FurniState };

export namespace FurniHelpers {
    export const buildResourceName = (
        itemName: string,
        size: WorldSize,
        layerId: number,
        direction: FurniDirection,
        frame: number
    ): string => {
        if (size === WorldSize.ICON) {
            return itemName + "_icon_" + getLayerName(layerId);
        }
        return itemName + "_" + size + "_" + getLayerName(layerId) + "_" + direction + "_" + frame;
    };

    export const getLayerName = (layerId: number) => {
        if (layerId === -1) {
            return "sd";
        }
        return String.fromCharCode(97 + layerId);
    };

    export const calculateStates = (visualization: FurniVisualiazation): FurniStateDictionary => {
        let states: FurniStateDictionary = { 0: { count: 1 } };

        if (visualization.animations != null) {
            for (let stateIdStr in visualization.animations) {
                const stateId = parseInt(stateIdStr);
                let count = 1;
                for (let animationLayer of visualization.animations[stateId].layers) {
                    if (animationLayer.frameSequence != null) {
                        let frameSequenceLength = 0;
                        animationLayer.frameSequence.forEach(sequence => {
                            frameSequenceLength += sequence.length;
                        });
                        if (count < frameSequenceLength) {
                            count = frameSequenceLength;
                        }
                    }
                }
                states[stateId] = { count };
                if (visualization.animations[stateId] != null) {
                    const { transitionTo } = visualization.animations[stateId];
                    if (transitionTo != null) {
                        const allegedTransition = transitionTo;
                        states[stateId].transitionTo = allegedTransition;
                        states[allegedTransition].transition = stateId;
                    }
                }
            }
        }
        return states;
    };
}