import TWEEN from "@tweenjs/tween.js";
import { IsometricPoint } from "hh_shared";
import { Renderer } from "pixi.js";
import { WorldConsts, WorldHelpers } from "../world/world-helpers";
import { HumanRendererOpts, HumanState, HumanRenderer, HumanActions } from "./human-renderer";
import { HumanLoader } from "./human-loader";
import { Game } from "../game";
import { RenderEngine } from "../renderer/render-engine";

export type HumanInstanceOpts = HumanRendererOpts & {
    instanceId: string,
    userId: string,
    name: string,
    initialPosition: IsometricPoint
};

export type HumanInstanceState = HumanState & {
    position?: IsometricPoint
};

export class HumanInstance extends HumanRenderer {
    private _position: IsometricPoint;
    private _activeTween: TWEEN.Tween = new TWEEN.Tween();
    private _instanceId: string;
    private _userId: string;
    private _name: string;

    constructor(
        opts: HumanInstanceOpts,
        state: HumanState
    ) {
        super(opts, state);
        this._position = opts.initialPosition;
        this._name = opts.name,
        this._instanceId = opts.instanceId;
        this._userId = opts.userId;
        this.setup();
    }

    setup() {
        Game.instance.renderer.visualStage.addChild(this.drawResult.visualContainer);
        Game.instance.renderer.logicStage.addChild(this.drawResult.logicContainer);
    }

    unload() {
        Game.instance.renderer.visualStage.removeChild(this.drawResult.visualContainer);
        Game.instance.renderer.logicStage.removeChild(this.drawResult.logicContainer);
    }

    setState(state: HumanInstanceState) {
        let actions: HumanActions = {};

        this._position = state.position;

        // Convert network actions to game actions
        for(const rawAction in state.actions) {
            const rawContents = state.actions[rawAction];
            switch(rawAction) {
                case "mv":{
                        const [x, y, z] =  rawContents[0].split(",");
                        const pos = IsometricPoint.of(parseInt(x), parseInt(y), parseFloat(z));
                        this._activeTween.stop();
                        this._activeTween = new TWEEN.Tween(this._position).to(pos, 500).start();
                        actions.wlk = [];
                    }
                    break;

                case "sit": {
                    this._position.z += parseFloat(rawContents[0]) - 1.0;
                    actions.sit = [];

                }
                break;

                case "lay": {
                    if(state.direction === 0) {
                        state.direction = 4;                        
                        state.headDirection = 4;
                    }
                    this._position.z += parseFloat(rawContents[0]) - 1.0;
                    actions.lay = [];

                }
                break;

            }
        }

        state.actions = actions;

        super.setState(state);
    }

    protected drawNextFrame() {
        super.drawNextFrame();
        this.updatePosition();
    }

    isHitDetected(selectedColor: number): boolean {
        if (selectedColor === this._logicColor) {
            return true;
        }

        return false;
    }

    private updatePosition() {
        const finalPos = this._position;

        const z = WorldHelpers.calculateZIndexFloorItem(finalPos, 0, WorldConsts.PRIORITIY_PLAYER, WorldConsts.OFFSET_PLAYER);
        const screenPos = WorldHelpers.tileToLocal(this._worldSize, finalPos);

        const logicContainer = this.drawResult.logicContainer;
        logicContainer.position.set(screenPos.x, screenPos.y + (this._worldSize / 4));
        logicContainer.zIndex = z;

        const visualContainer = this.drawResult.visualContainer;
        visualContainer.position.set(screenPos.x, screenPos.y + (this._worldSize / 4));
        visualContainer.zIndex = z;
    }
}