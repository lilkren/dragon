import { ServerMessage } from "../server-message";
import { MessageDeserializer } from "../../../util/message-deserializer";

export class Heightmap extends ServerMessage {
    heightmap: string;

    constructor(deserializer: MessageDeserializer) {
        super(ServerMessage.HEIGHTMAP, deserializer);
    }

    decode() {
       this.heightmap = this._deserializer.peekBody();
    }
}