import * as fs from "fs";
const { readFromBufferP, extractImages } = require('swf-extract');
const SWFReader = require('@gizeta/swf-reader');

export namespace SWFUtils {
    export const extractSwf = async (swfPath: string, outputPath: string, trimLength: number = 0) => {
        const rawData = fs.readFileSync(swfPath);
        const swfObject = SWFReader.readSync(swfPath);
        const assetMap = extractGenerateAssetMap(swfObject);
        extractXmls(swfObject, assetMap, outputPath, trimLength);

        const swf = await readFromBufferP(rawData);
        const images = await extractImages(swf.tags);
        for (const image of images) {
            const finalImage = await image;
            for (const name of assetMap[finalImage.characterId]) {
                const fileName = name.substring(trimLength) + ".png";
                fs.writeFileSync(outputPath + "/" + fileName, finalImage.imgData, "binary");
            }
        }
    }

    export const extractGenerateAssetMap = (swf: any) => {
        const assetMap: any = {};
        for (let _i = 0, _a = swf.tags; _i < _a.length; _i++) {
            const tag = _a[_i];
            if (tag.header.code == 76) {
                for (let _b = 0, _c = tag.symbols; _b < _c.length; _b++) {
                    const asset = _c[_b];
                    if(!assetMap[asset.id]) assetMap[asset.id] = [];
                    assetMap[asset.id].push(asset.name);
                }
            }
        }
        return assetMap;
    }

    export const extractXmls = (swf: any, assetMap: any, folderName: string, trimLength: number) => {
        for (let _i = 0, _a = swf.tags; _i < _a.length; _i++) {
            const tag = _a[_i];
            if (tag.header.code == 87) {
                const buffer = tag.data;
                const characterId = buffer.readUInt16LE();
                for (const name of assetMap[characterId]) {
                    const fileName = name.substring(trimLength) + ".xml";
                    fs.writeFileSync(folderName + "/" + fileName, buffer.subarray(6), "binary");
                }
            }
        }
    }
}